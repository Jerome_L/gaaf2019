# gaaf2019

scripts R à partager pour le gaaf2019 : https://www.gaaf-asso.fr/rencontres/typochronologie-tombe-inhumation/

Les scripts sont rpartis dans des dossiers:

## Gantt

Script pour faire un diagramme de Gantt

## densite de probabilite par periode

Script pour faire un tableau et un graphique (barplot) des densités de probabilité des types de tombes selon un pas chronologique défini

## Gsheet-R-PostGis

Script permettant de lire le tableau d'inventaire des tombes depuis une table (Google Sheet) 